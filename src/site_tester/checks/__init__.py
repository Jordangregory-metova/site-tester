"""
Checks various things about a given URL.
"""

import requests


class Exceptions(Exception):
    pass


class URLNotProvided(Exception):
    pass


class HTTPClientError(Exception):
    pass


class HTTPServerError(Exception):
    pass


http_100 = [
    100, 101, 102, 103,
]

http_200 = [
    200, 201, 202, 203, 204, 205, 206, 207, 208, 226,
]

http_300 = [
    300, 301, 302, 303, 304, 305, 306, 307, 308
]

http_400 = [
    400, 401, 402, 403, 404, 405, 406, 407, 408, 409, 410, 411, 412, 413, 414, 415, 416, 417, 418, 421, 422, 423, 424,
    426, 428, 429, 431, 451
]

http_500 = [
    500, 501, 502, 503, 504, 505, 506, 507, 508, 510, 511
]

positive_http_status_codes = [
    http_100, http_200, http_300
]


class Availability:
    """
    Checks if the given URL is available.
    """
    def __init__(self, url=None):
        self.url = url
        if url is None:
            raise URLNotProvided

    def is_available(self):
        try:
            r = requests.get(url=self.url)
            if r.status_code in positive_http_status_codes:
                return True
            elif r.status_code in http_400:
                raise HTTPClientError
            elif r.status_code in http_500:
                raise HTTPServerError
        except Exception as e:

            return False
