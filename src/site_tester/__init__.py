import os
import logging

import yaml


class InvalidConfigFile(Exception):
    pass


class App:
    def __init__(self, configFile=None):
        self.configFile = configFile
        if configFile is not None:
            self._parse_config()
            self._parse_checks_config()
            self._parse_tests_config()
            self.log()

    def _parse_config(self):
        try:
            with open(self.configFile) as stream:
                with yaml.load(stream) as y:
                    base = y['site_tester-config']
                    self.loggingConfig = base['logging']
                    self.checkConfig = base['check-config']
                    self.testConfig = base['test-config']

                    self.logFile = self.loggingConfig['log-file']
                    if f"{self.loggingConfig['log-level']}".upper() == 'DEBUG':
                        self.logLevel = logging.DEBUG
                    elif f"{self.loggingConfig['log-level']}".upper() == 'INFO':
                        self.logLevel = logging.INFO
                    elif f"{self.loggingConfig['log-level']}".upper() == 'WARNING':
                        self.logLevel = logging.WARNING
                    elif f"{self.loggingConfig['log-level']}".upper() == 'CRITICAL':
                        self.logLevel = logging.CRITICAL
        except:
            raise InvalidConfigFile

    def _parse_checks_config(self):
        pass

    def _parse_tests_config(self):
        pass

    def log(self):
        log = logging.Logger(__name__)
        log.setLevel(self.logLevel)
        fh = logging.FileHandler(self.logFile)
        log.addHandler(fh)
        return log

    def run(self):
        pass