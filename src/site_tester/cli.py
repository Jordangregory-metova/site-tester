# STD LIB
import argparse

# 3RD PARTY
# PLACEHOLDER

# LOCAL
from . import App


def parse_args():
    p = argparse.ArgumentParser()

    p.add_argument(
        '--config-file', '-c',
        dest='config',
        help='Tests the UP/DOWN status of http/https of the URL provided',
        required=True,
    )

    return p.parse_args()


def run():
    args = parse_args()
    app = App(configFile=args.conifg)
    app.run()


if __name__ == "__main__":
    run()
