from setuptools import setup


setup(
    name='site_tester',
    version='0.1.0',
    description='Tests various things about websites based on a config.',
    long_description='',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3.7',
    ],
    url='',
    author='Jordan Gregory',
    author_email='jordan.gregory@mevota.com',
    license='MIT',
    packages=[
        'site_tester',
        'site_tester.checks'
    ],
    package_dir={'':'src'},
    install_requires=[
        'aiohttp',
        'requests',
        'pyyaml'
    ],
    setup_requires=[
        'pytest-runner',
    ],
    tests_requires=[
        'pytest'
    ],
    include_package_data=True,
    zip_save=False,
    entry_points={
        'console_scripts': [
            'site-tester=site_tester.cli:run'
        ]
    }
)
